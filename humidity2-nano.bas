time VAR Word
humidity VAR Word
rhconstant CON 12190

loop
HIGH p2
PAUSE 10
adin p2, time

serout s_out, i9600, [0, "Time = ", dec time]
time = time / 10
humidity = (time - rhconstant) / 24
serout s_out, i9600, [13, "Relative Humidity = ", DEC humidity, "%"]
PAUSE 200
goto loop