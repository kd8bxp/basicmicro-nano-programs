time VAR Word
humidity VAR Word
rhconstant CON 727

loop
HIGH p2
PAUSE 10
RCTIME p2, 1, time

time = 90
serout s_out, i9600, [0, "Time = ", dec time]
'time = time * 10
humidity = time + rhconstant / 24
serout s_out, i9600, [13, "Relative Humidity = ", DEC humidity, "%"]
PAUSE 200
goto loop