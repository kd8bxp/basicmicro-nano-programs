;This example shows reading a digital pin state using the button command.

;Connect one side of a button to P0 of the processor.
;Connect the other side of the button to gnd.
;Connect a 10k resistor from P0 to Vcc

work var byte

main
	button p0,0,100,20,work,1,dosomethingelse
	serout s_out,i9600,["Do something:",dec work,13]
	goto main

dosomethingelse
	serout s_out,i9600,["Do something else",13]
	goto main
