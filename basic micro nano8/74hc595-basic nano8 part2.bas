ser con p0
srck con p1
rck con p2
delay con 50
counter var byte

main:

counter = %10000000
pause delay
gosub out595
counter = %01000000
pause delay
gosub out595
counter = %00100000
pause delay
gosub out595
counter = %00010000
pause delay
gosub out595
counter = %00001000
pause delay
gosub out595
counter = %00000100
pause delay
gosub out595
counter = %00000010
pause delay
gosub out595l

goto main

out595:
shiftout ser,srck,msbpost,[counter]
pulsout rck,8
return

out595l:
shiftout ser,srck,lsbpost,[counter]
pulsout rck,8
return